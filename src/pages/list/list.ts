import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import { ToastController ,ItemSliding} from 'ionic-angular';
import { ItemDetailsPage } from '../item-details/item-details';

import {AngularFirestore,AngularFirestoreCollection} from 'angularfire2/firestore';
import { Usuario } from '../../clases/usuario';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;
  coleccionTipadaFirebase:AngularFirestoreCollection<Usuario>;
  ListadoDeUsuarios:Usuario[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private objFirebase: AngularFirestore,private toastCtrl: ToastController ) {
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];
    for(let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
  }
  ionViewDidEnter(){
    this.coleccionTipadaFirebase= this.objFirebase.collection<Usuario>('usuarios', ref=> ref.orderBy('nombre'));
    this.coleccionTipadaFirebase.snapshotChanges().subscribe(listadoUsuario => {
      this.ListadoDeUsuarios= listadoUsuario.map(usuarioRetornado=>{
        console.clear();
        console.info("usuario",  usuarioRetornado.payload.doc.data().foto);
        let nuevoUsuario:Usuario;
          nuevoUsuario = new Usuario( usuarioRetornado.payload.doc.data().nombre,
            usuarioRetornado.payload.doc.data().clave,
            usuarioRetornado.payload.doc.id, 
            usuarioRetornado.payload.doc.data().foto);
            nuevoUsuario.estado=usuarioRetornado.payload.doc.data().estado;
            nuevoUsuario.perfil=usuarioRetornado.payload.doc.data().perfil;
        return nuevoUsuario;

      })
       console.info("listado de usuarios", this.ListadoDeUsuarios);
    })
     console.log("fin de ionViewDidEnter");

  }
  mostrarUsuario(event, item:Usuario) {
    this.navCtrl.push(ItemDetailsPage, {
      item
    });
  }


  bloquear(item: ItemSliding) {
    this.MensajeDeAccion(item, 'bloqueando', 'usuario bloqueado.');
  }
   activar(item: ItemSliding) {
    this.MensajeDeAccion(item, 'activando', 'usuario activado.');
  }
   silenciar(item: ItemSliding) {
    this.MensajeDeAccion(item, 'silenciando', 'usuario silenciado.');
  }
  borrar(item: ItemSliding) {
    this.MensajeDeAccion(item, 'borrando', 'usuario borrado.');
  }

  nuevoUsuario(evento){
    console.log("nuevo usuario");
  }
   MensajeDeAccion(item: ItemSliding, _: any, text: string) {
    // TODO item.setElementClass(action, true);
    setTimeout(() => {
      const toast = this.toastCtrl.create({
        message: text+_
      });
      toast.present();
      // TODO item.setElementClass(action, false);
      item.close();

      setTimeout(() => toast.dismiss(), 2000);
    }, 1500);
  }
}
