import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

/**
 * Generated class for the ConfiguracionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configuracion',
  templateUrl: 'configuracion.html',
})
export class ConfiguracionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams ,public event:Events) {
  }
  rotarTema() {
    this.event.publish('rotar:tema');

  }
  cambiarTema(tema:string) {
    this.event.publish('cambiar:tema',tema);

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfiguracionPage');
  }

}
