import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav ,Events} from 'ionic-angular';

import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ConfiguracionPage } from '../pages/configuracion/configuracion';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage = HelloIonicPage;
  pages: Array<{title: string, component: any}>;
  temasVigentes: Array<{nombre: string}>;
  temaActual:String = 'tema-sandro';
  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public event: Events
  ) {
    this.initializeApp();

    // set our app's pages
    this.pages = [
      { title: 'Hello Ionic', component: HelloIonicPage },
      { title: 'My First List', component: ListPage },
       { title: 'Configuración', component: ConfiguracionPage }
    ];
      this.temasVigentes = [
      { nombre: 'tema-lima-limon' },
      { nombre: 'tema-sandro' },
      { nombre: 'tema-profecional' },
      { nombre: 'tema-vacio' }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

     this.event.subscribe('cambiar:tema', (estilo) => {
        this.cambiarTema(estilo);
      });
      this.event.subscribe('rotar:tema', () => {
        this.rotarTemas();
      });



    });


  }
  cambiarTema(tema:string) {
   
      this.temaActual =tema;
   
  }
   rotarTemas() {
    
 console.info("tema actual "+ this.temaActual);
   for (var i = 0; i < this.temasVigentes.length ; i++) {
   
    console.info("temas vigentes "+ this.temasVigentes[i].nombre);

     if(this.temaActual==this.temasVigentes[i].nombre &&  i <this.temasVigentes.length   )
     {
       this.temaActual=this.temasVigentes[i+1].nombre;
       break;
     }
     else{
        if(this.temaActual==this.temasVigentes[i].nombre &&  i ==this.temasVigentes.length -1)
         {
           this.temaActual=this.temasVigentes[0].nombre;
           break;
         }
     }
  
    }
     console.log("sale con : "+this.temaActual);
 }
  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}
